(function($) {
    var firstPass = true;
    var lastWasSuccess = true;
    $.fn.inputEval = function(input, condition, format, furtherFunctionality, config, nonPassingConditionIsFailure, inputType) {

        var it = input.attr("axa-input");
        var skipValidation = false;
        if (!it) skipValidation = true;
        if (it && it.search($.fn.inputEval.INPUT_TYPE_ENUM.CONDITIONAL_FIELD) > -1) {
            //this is special check for conditional fields, if they havent passed then none of their validations should happen
            var pcattr = localEval(input.attr("passedcondition"));
            if (pcattr == null || pcattr == "") {
                if (config.inputerrorclass) {
                    input.removeClass(config.inputerrorclass);
                }
                //still may want to run this
                if (furtherFunctionality) furtherFunctionality.apply(null);
                firstPass = true;
                skipValidation = true;
            }
        }

        if (!skipValidation)
            if (condition) {
                var before = input.val();
                if (format) {
                    var val = format();
                    if (config.automutatevalue) {
                        if (val != null) input.val(val);
                    }
                }
                //this functionality will always be run, and run after the value is set, if it is set
                if (furtherFunctionality) furtherFunctionality(input, config);

                //revalidate the field
                if (nonPassingConditionIsFailure) {
                    removeInvalidatedFieldFromFormIndex(input, inputType);

                    //modify the view based on things sent in the cofig
                    if (config.inputerrorclass && !hasInvalidatedReasons(input)) {
                        input.removeClass(config.inputerrorclass);
                    }
                    if (config.errortextdiv && !hasInvalidatedReasons(input)) {
                        //if(config.errortext){
                        var doAnimation = true;
                        $("#" + config.errortextdiv).text("");
                        if ($("#" + config.errortextdiv).attr("lastanimationtrigger") == "success") doAnimation = false;
                        $("#" + config.errortextdiv).attr("lastanimationtrigger", "success");
                        if (doAnimation) {
                            $("#" + config.errortextdiv).stop("axaFX", true, true);
                            $("#" + config.errortextdiv).animate({
                                //height : 0,
                                opacity: 0
                            }, {
                                duration: config.animateerrortextdiv ? 150 : 0,
                                queue: "axaFX",
                                easing: "linear" //,
                                    //complete : function(){$("#" + config.errortextdiv).hide()}
                            })
                            $("#" + config.errortextdiv).dequeue("axaFX");
                        }
                        //}
                    }
                }

                //callback that the field is valid
                if (config.passcallback && !hasInvalidatedReasons(input) && (input.attr("isPassing") == "false" || input.attr("isPassing") == null)) {
                    input.attr("isPassing", "true");
                    config.passcallback.apply(null, [input, before, val, inputType]);
                }

            } else {
                //invalid and should be considered a failure
                if (nonPassingConditionIsFailure) {
                    //invalidate the field
                    addInvalidatedFieldToFormIndex(input, inputType);
                    if (config.failcallback && (input.attr("isPassing") == "true" || input.attr("isPassing") == null)) {
                        input.attr("isPassing", "false");
                        config.failcallback(input, inputType, inputType);
                    }

                    //modify the view based on things sent in the cofig
                    if (config.inputerrorclass) {
                        input.addClass(config.inputerrorclass);
                    }
                    if (config.errortextdiv) {
                        if (config.errortext) {
                            $("#" + config.errortextdiv).text(config.errortext);
                            var doAnimation = true;
                            var etd = $("#" + config.errortextdiv);
                            if (etd.attr("lastanimationtrigger") == "error") doAnimation = false;
                            if (doAnimation) {
                                etd.attr("lastanimationtrigger", "error");
                                etd.stop("axaFX", true, true);
                                etd.show()
                                etd.animate({
                                    opacity: 1
                                }, {
                                    duration: config.animateerrortextdiv ? 150 : 0,
                                    queue: "axaFX",
                                    easing: "linear"
                                })
                                $("#" + config.errortextdiv).dequeue("axaFX");
                            }
                        }
                    }

                } else {
                    //Should technically turn into a pass cuz it doesnt fail anything and everything was entered correctly by user
                    $.fn.inputEval(input, true, format, furtherFunctionality, config, nonPassingConditionIsFailure, inputType);
                }

            }

            //why does this line need to be here or it doesnt work?
            //is it some kind of race condition?
            //if this line isnt here it only works when there is a breakpoint
            //but delays dont work anyway
            //ijdgiaa
        var test = $.fn.getInvalidFormElements(input);

        //revalidate the form if possible
        if (config.formvalidatedcallback && getIsFormValidByInput(input) && !hasInvalidatedReasons(input) && (!lastWasSuccess || firstPass)) {
            lastWasSuccess = true;
            firstPass = false;
            config.formvalidatedcallback(getFormByInput(input));
        } else //invalidate form
        if ((config.forminvalidatedcallback && (lastWasSuccess || firstPass) && $.fn.getInvalidFormElements(input).length > 0)) {
            lastWasSuccess = false;
            firstPass = false;
            config.forminvalidatedcallback(getFormByInput(input), getInvalidatedInputsByInput(input));
        }
        firstPass = false;
    };

    //PUBLIC ENUMS
    $.fn.inputEval.TRIGGER_ENUM = {
        KEY_UP: "keyup",
        KEY_DOWN: "keydown",
        FOCUS_OUT: "focusout",
        ANY_CHANGE: "propertychange change keyup input paste"
    };

    //PUBLIC ENUMS
    //Enumeration Types
    $.fn.inputEval.INPUT_TYPE_ENUM = {
        TO_UPPER_CASE: "touppercase",
        TO_LOWER_CASE: "tolowercase",
        MAX_LENGTH: "maxlength",
        MIN_LENGTH: "minlength",
        VALIDATE_EMAIL: "validateemail",
        MANDATORY: "mandatory",
        ONLY_NUMBERS: "onlynumbers",
        MATCH_FIELD: "matchfield",
        CONDITIONAL_FIELD: "conditionalfield",
        CALL_SERVICE: "callservice"
    };

    $.fn.registerValidationFunction = function(key, func) {
        if (!$.fn.registerValidationFunction.dataStore) $.fn.registerValidationFunction.dataStore = {};
        for(var p in $.fn){
        	if($.fn[p] == func){
        		//indexing method names so we dont have to do it on every pass
        		$.fn.registerValidationFunction.dataStore[key] = {"fnName" : p, "func" : func};
        		break;
        	}
        	
        };
    }

    function evalHandler(e, passedInput, toSkip, config) {
        var input = e ? $(e.currentTarget) : passedInput;
        var inputType;
        if (input) {
            if (input.attr("axa-input") != null) {
                inputType = input.attr("axa-input");
            } else {
            	//Since on radio buttons, only one of them have been tagged as an axa-input, if another one is clicked we need to search for the actual input
                var inputString = "input[name=" + input.attr("name") + "]";
                var collection = $(inputString);
                $.each(collection, function(index, item) {
                    if ($(item).attr("axa-input")) {
                        input = $(item);
                        inputType = $(item).attr("axa-input");
                    }
                })
            }
        }
        if (input && inputType) {
            //allow to pass multiple types in here
            var dict = $.fn.registerValidationFunction.dataStore;
            $.each(dict, function(val, index) {
            	if (inputType.search(val) > -1){
            		if(toSkip == val){
            			if((inputType.search(toSkip) < 0 || toSkip == null)){
            				input[index.fnName].apply(input, [config || {}]);
            			}
            		}else{
            			input[index.fnName].apply(input, [config || {}]);
            		}
            	}
            })

        }
    }

    /*////////////////////////////////////////////////////
     *\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     *The following properties may be set on any config or on any input - 
     *they will both be merged into the master config and applied at a field level
     *triggerevent - the event to trigger change on, default is any change - but for things like kanji its better to do focusout
     *automatevalue - will write values automatically back to the field after validation DEFAULT TRUE
     *continuewatch - will keep watching javascript fields after first call DEFAULT TRUE
     *preventinvalidatedformsubmissionwithreturnkey - will stop form entry by enter button if form is invalid DEFAULT TRUE
     *inputerrorclass - css class to be applied to the input upon error
     *errortextdiv - div name to write error text to
     *animateerrortextdiv - this will animate open and shut the div when errors come and go
     *errortext - the value of the text to be displayed in the div
     *However the only configs that will actually trigger an error state are ones that actually cause failure on the form
     *Those are - mandatory, minlength, email
     *The rest of them do conversion and force values on fields therefore a user cannot enter a faulty value
     *\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     *\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
     */


    $.fn.mandatory = function(config) {
        //no specific local configs for this
        //super config can be updated via node or config param
        var input = $(this);
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.MANDATORY);
        var radioIsChecked = false;
        var inputString = "input[name=" + input.attr("name") + "]";
        checkPrecondition(input.attr("type") != null, "Your input has no type");
        switch (input.attr("type")) {
            case "input":
                $.fn.inputEval(input, input.val().length > 0, null, null, config, true, $.fn.inputEval.INPUT_TYPE_ENUM.MANDATORY)
                break;
            case "radio":
                //if(parent.prop("checked") == true){
                $.each($(inputString), function(index, item) {
                    if ($(item).prop("checked") == true) {
                        radioIsChecked = true;
                    }
                    $.fn.inputEval(input, radioIsChecked, null, null, config, true, $.fn.inputEval.INPUT_TYPE_ENUM.MANDATORY)
                });
                break;
        }
        return input;
    }

    $.fn.toUpperCase = function(config) {
        //no specific local configs for this
        //super config can be updated via node or config param
        var input = $(this);
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.TO_UPPER_CASE);
        var hasLowerCase = swapCases(input, $.fn.inputEval.INPUT_TYPE_ENUM.TO_UPPER_CASE);
        $.fn.inputEval(input, hasLowerCase, function() {
            return input.val().toUpperCase()
        }, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.TO_UPPER_CASE)
        return input;
    }

    $.fn.toLowerCase = function(config) {
        //no specific local configs for this
        //super config can be updated via node or config param
        var input = $(this);
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.TO_LOWER_CASE);
        var hasUpperCase = swapCases(input, $.fn.inputEval.INPUT_TYPE_ENUM.TO_LOWER_CASE);
        $.fn.inputEval(input, hasUpperCase, function() {
            return input.val().toLowerCase()
        }, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.TO_LOWER_CASE)
        return input;
    }

    //reduce duplication
    function swapCases(input, type) {
        var i = 0;
        var has = false;
        for (i; i < input.val().length; i++) {
            var chr = input.val().substr(i, 1);
            if (type == $.fn.inputEval.INPUT_TYPE_ENUM.TO_UPPER_CASE) {
                if (chr != chr.toUpperCase()) {
                    has = true;
                    break;
                }
            } else if (type == $.fn.inputEval.INPUT_TYPE_ENUM.TO_LOWER_CASE) {
                if (chr != chr.toLowerCase()) {
                    has = true;
                    break;
                }
            }
        }
        return has;
    }


    $.fn.maxLength = function(config) {
        var input = $(this);
        var localConfig = {
            maxcharacterlength: null,
            itemtofocuswhenmaxlengthreached: null
        };
        config = $.extend(localConfig, config);
        if (config.maxcharacterlength == null) delete config.maxcharacterlength;
        if (config.itemtofocuswhenmaxlengthreached == null) delete config.itemtofocuswhenmaxlengthreached;
        //super config can be updated via node or config param
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.MAX_LENGTH);
        checkPrecondition(config.maxcharacterlength != null, "Must define maxcharacterlength");
        var func = function() {
            return input.val().substr(0, config.maxcharacterlength);
        };
        $.fn.inputEval(input, input.val().length >= config.maxcharacterlength, func,
            function(input, config) {
                if (config.itemtofocuswhenmaxlengthreached && config.maxcharacterlength == input.val().length && mostRecentKey != 9 && mostRecentKey != 37 && mostRecentKey != 38 && mostRecentKey != 39 && mostRecentKey != 40) {
                    var field = $("#" + config.itemtofocuswhenmaxlengthreached);
                    field.focus();
                }
            }, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.MAX_LENGTH)
    }

    $.fn.minLength = function(config) {
        var input = $(this);
        var localConfig = {
            mincharacterlength: null
        };
        config = $.extend(localConfig, config);
        if (config.mincharacterlength == null) delete config.mincharacterlength;
        //super config can be updated via node or config param
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.MIN_LENGTH);
        checkPrecondition(config.mincharacterlength != null, "Must define mincharacterlength");

        $.fn.inputEval(input, input.val().length >= config.mincharacterlength, function() {
            return input.val()
        }, null, config, true, $.fn.inputEval.INPUT_TYPE_ENUM.MIN_LENGTH)
        return input;
    }


    $.fn.validateEmail = function(config) {
        var input = $(this);
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.VALIDATE_EMAIL);
        var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        var valid = emailReg.test(input.val());
        $.fn.inputEval(input, valid, null, null, config, true, $.fn.inputEval.INPUT_TYPE_ENUM.VALIDATE_EMAIL);
        return input;
    }

    $.fn.onlyNumbers = function(config) {
        var input = $(this);
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_NUMBERS);
        var numberReg = new RegExp(/\[0-9]/g);
        var val = input.val();
        var valid = numberReg.test(val);
        var cleaned = val.replace(/\D+/g, '');
        $.fn.inputEval(input, !valid, function() {
            return cleaned
        }, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.ONLY_NUMBERS);
        return input;
    }


    $.fn.matchField = function(config) {
        var input = $(this);
        var localConfig = {
            fieldtomatch: null
        };
        config = $.extend(localConfig, config);
        if (input.attr('fieldtomatch')) {
            config.fieldtomatch = localEval(input.attr('fieldtomatch'));
        }
        if (config.fieldtomatch == null) delete config.fieldtomatch;
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.MATCH_FIELD);
        var val = input.val();
        var field2 = $("#" + config.fieldtomatch);
        field2.off(config.triggerevent, matchFieldTriggerFunction);
        field2.on(config.triggerevent, {input : input, config : config}, matchFieldTriggerFunction);
        var val2 = field2.val();
        $.fn.inputEval(input, val == val2 && val != null && val != "" && val2 != null && val2 != "", null, null, config, true, $.fn.inputEval.INPUT_TYPE_ENUM.MATCH_FIELD);
        evalHandler(null, input, $.fn.inputEval.INPUT_TYPE_ENUM.MATCH_FIELD);
        return input;
    }
    //need to trigger when the match field changes
    function matchFieldTriggerFunction(e){
    	e.data.input.matchField(e.data.config);
    }
    


    var previousCall;
    $.fn.callService = function(config) {
        var input = $(this);
        var localConfig = {
            "axa-mutationmethod": null, //not mandatory
            uri: null, //mandatory
            uritoken: "%%", //this token will be replaced with the value of the text field, or value after mutation (if mutationmethod is defiend)
            "axa-successcallback": null,
            "axa-failurecallback": null
        };
        config = $.extend(localConfig, config);
        if (input.attr('uritoken')) {
            config.uritoken = localEval(input.attr('uritoken'));
        }
        if (config["axa-mutationmethod"] == null) delete config["axa-mutationmethod"];
        if (config.uri == null) delete config.uri;
        if (config.uritoken == null) delete config.uritoken;
        if (config["axa-successcallback"] == null) delete config["axa-successcallback"];
        if (config["axa-failurecallback"] == null) delete config["axa-failurecallback"];
        //super config can be updated via node or config param
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.CALL_SERVICE);
        checkPrecondition(config["uri"] != null, "Must define uri");
        var val = input.val();
        var mutationFN = config["axa-mutationmethod"];
        if (mutationFN) {
            mutationFN = parse(mutationFN, "axa-mutationmethod");
            val = mutationFN(val); // || val;
        }
        //only call a service if a non null value is passed - this allows us to return null in our mutationmethod if we dont want to search
        if (val && val != "") {
            var re = new RegExp(config.uritoken, 'gi');
            var uri = config.uri.replace(re, val);
            if (previousCall) previousCall.abort();
            var req = previousCall = $.ajax(uri)
                .done(function(evt) {
                    if (config["axa-successcallback"]) parse(config["axa-successcallback"], "axa-successcallback")(evt);
                })
                .fail(function(err) {
                    if (config["axa-failurecallback"]) parse(config["axa-failurecallback"], "axa-failurecallback")(err);
                })
                .always(function() {
                    previousCall = null;
                });
        } else {
            //should fail the first time
            if (config["axa-failurecallback"]) parse(config["axa-failurecallback"], "axa-failurecallback")(null);
        }
        return input;
        //$.fn.inputEval(input, false, function(){return val}, null, config, false, $.fn.inputEval.INPUT_TYPE_ENUM.CALL_SERVICES);
    }

    $.fn.conditionalField = function(config) {
        var input = $(this);
        var localConfig = {
            parentfield: null,
            conditionalvalue: null,
            animateconditionalfield: true
        };
        config = $.extend(localConfig, config);
        if (config.parentfield == null) delete config.parentfield;
        if (config.conditionalvalue == null) delete config.conditionalvalue;
        //super config can be updated via node or config param
        config = getLocalConfig(input, config, $.fn.inputEval.INPUT_TYPE_ENUM.CONDITIONAL_FIELD);
        checkPrecondition(config.parentfield != null, "Must define parentfield");
        checkPrecondition(config.conditionalvalue != null, "Must define conditionalvalue");

        var parent = $("#" + config.parentfield);
        var inputString = "input[name=" + parent.attr("name") + "]";
        var isChangeRegistered = localEval(input.attr("changeregistered"));
        switch (parent.attr("type")) {
            case "input":
                if (!isChangeRegistered) {
                    parent.on($.fn.inputEval.TRIGGER_ENUM.ANY_CHANGE, (function(e) {
                        if (parent.val() == config.conditionalvalue) {
                            input.attr("passedcondition", "true");
                        } else {
                            input.attr("passedcondition", null);
                            removeInvalidatedReasons(input);
                        }
                        doPreconditionStuff(config, input, parent);
                    }));
                    if (parent.val() == config.conditionalvalue)jQuery.data(input,"passedCondition", true);// input.attr("passedcondition", "true"); //
                    doPreconditionStuff(config, input, parent);
                }
                break;
            case "radio":
                if (!isChangeRegistered) {
                    $(inputString).change(function(e) {

                        e.stopPropagation();
                        if (parent.prop("checked") == true) {
                            input.attr("passedcondition", "true");
                        } else {
                            input.attr("passedcondition", null);
                            removeInvalidatedReasons(input);
                        }
                        doPreconditionStuff(config, input, parent);
                    });
                    if (parent.prop("checked") == true)input.attr("passedcondition", "true"); //
                    doPreconditionStuff(config, input, parent);
                }
                break;
            case "checkbox":
                if (!isChangeRegistered) {
                    parent.change(function(e) {

                        if (parent.prop("checked") && parent.val() == config.conditionalvalue) {
                            input.attr("passedcondition", "true");
                        } else {
                            input.attr("passedcondition", null);
                            removeInvalidatedReasons(input);
                        }
                        doPreconditionStuff(config, input, parent);
                    });
                    if (parent.prop("checked") == true && parent.val() == config.conditionalvalue)input.attr("passedcondition", "true"); //
                    doPreconditionStuff(config, input, parent);
                }
                break;
        }
        input.attr("changeregistered", "true");
        //jQuery.data(input,"changeRegistered", true);
        return input;
    }
    
    //utility method to reduce duplication
    function doPreconditionStuff(config, input, parent) {
        var pc = localEval(input.attr("passedcondition"));
        var etd;
        if(config.errortextdiv)etd = $("#"+config.errortextdiv);
        if (pc == true) {
        	if(etd)etd.show();
            input.show();
            evalHandler(null, input, "conditionalfield");
            return;
        } else {
        	if(etd)etd.hide();
        	input.hide();
            evalHandler(null, input, "conditionalfield");
        }
    }

    function parse(strFn, propName) {
        try {
            var whole = String(strFn).split("{");
            var sig = whole[0];
            sig = sig.replace(/ /g, "");
            sig = sig.replace(/function\(/g, "");
            sig = sig.replace(/\)/g, "");
            var arg = sig.split(",")
            var body = whole[1];
            body = body.split("}");
            body.pop();
            var all = arg.concat(body);
            var fn = Object.create(Function.prototype);
            var constructor = fn.constructor.apply(fn, all);
            return constructor;
        } catch (err) {
            throw propName + " has malformed javascript passed - " + err;
        }
    }

    //fine to pass either a child element of the form or the form itself
    $.fn.getInvalidFormElements = function(formOrElement) {
        firstPass = true;
        var theForm = getFormByInput($(formOrElement));
        if (!theForm) {
            //we have to assume they just passed us the form
            theForm = formOrElement;
        }
        return getInvalidatedInputsByInput(null, theForm);
    }

    $.fn.getFormElementInvalidReasons = function(input) {
        firstPass = true;
        if (hasInvalidatedReasons($(input))) {
            var arr = [];
            for (var prop in invalidReasons[getUniqueID($(input))]) {
                arr.push(prop);
            }
            return arr;
        } else {
            return [];
        }
    }



    //Global Config
    $.fn.inputEval.config = {
        triggerevent: $.fn.inputEval.TRIGGER_ENUM.ANY_CHANGE,
        passcallback: null,
        failcallback: null,
        forminvalidatedcallback: null,
        formvalidatedcallback: null,
        automutatevalue: true,
        continuetowatch: true, //this is to be used by calls from javascript - if they set this to fault they will have to call the methods every time a change happens - if they keep it true it will be managed fo them
        preventinvalidatedformsubmissionwithreturnkey: true //if there is an invalidated form it will attempt to stop form submission
    };


    //utilities
    function mapAttributeValuesToObject(node) {
        var obj = {};
        $.each(node.attributes, function(i, attribute) {
            obj[attribute.name] = localEval(attribute.value);
        })
        return obj;
    }
    
    //global access
    $.fn.localEval = function(value){
    	return localEval(value);
    }

    function localEval(value) {
        switch (value) {
            case "true":
                return true;
            case "false":
                return false;
            case "null":
                return null;
            default:
                //only parse our prefixed functions
                if (String(value).search("function") >= 0 && String(value).search("axa-") >= 0) return parse(value);
                return value;
        }
    }

    //refactoring - create externally accessible method
    $.fn.getLocalConfig = function(node, extra, inputType) {
        return getLocalConfig(node, extra, inputType);
    }

    function getLocalConfig(node, extra, inputType) {
        var mappedAttributes = mapAttributeValuesToObject(node[0]);
        var toReturn = $.extend({}, $.fn.inputEval.config, mappedAttributes, extra);
        if (extra) writeToAttributes(toReturn, node);
        //all methods call this so to remove duplication we will call continue watch from here
        buildFormIndexes(node);
        continueWatch(node, toReturn, inputType, true)
        return toReturn;
    }

    function checkPrecondition(condition, message) {
        if (!condition) {
            throw message;
        }
    }

    function writeToAttributes(config, input) {
        $.each(config, function(index, value) {
            if (index != "axa-input") {
                //we could write the functions to the attributes, but parsing them back and forth is expensive
                //so for now, if you write an extension that you want functions parsed, prefix it with axa- and it will parse them
                var strVal = String(value);
                if (strVal.search("function") >= 0) {
                    if (String(value).search("axa-") >= 0) {
                        input.attr(index, String(value));
                    }
                } else {
                    input.attr(index, String(value));
                }
            }
        });
    }

    //this is the utility method that will help us keep watching inputs even if they are added by javascript
    //it can also stop watching as well - this all happens by adding and removing attributes
    function continueWatch(input, config, inputType, val) {
        if (input && config.continuetowatch && val) {
            var axaInputVal = input.attr("axa-input");
            if (axaInputVal == null || axaInputVal == undefined || axaInputVal == "" || axaInputVal == " ") {
                if (inputType != undefined && inputType != null) input.attr("axa-input", inputType);
            } else {
                if (inputType != undefined && inputType != null) {
                    input.attr("axa-input", input.attr("axa-input") + " " + inputType);
                }
            }
        } else {
            if (input) {
                input.attr("axa-input", null);
                input.off(config.triggerevent, evalHandler);
            }
        }
        //finally, lets see if the trigger event has changed
        doEventListeners(input, config);
    }

    var formDictionary = {};
    var invalidatedFormMembers = {};
    var invalidReasons = {};

    function buildFormIndexes(input) {
        var parent = getParentForm(input);
        if (parent && parent.length > 0 && !input.attr("axa-input-id")) {
            setUniqueID(input);
            setUniqueID(parent);
            if (!formDictionary[getUniqueID(parent)]) {
                //store the form in a dictionary
                formDictionary[getUniqueID(parent)] = parent;
                //build child dictionary
                invalidatedFormMembers[getUniqueID(parent)] = {};
            }
        } else {
            //this means that we have found an input that doesnt belong to a form - maybe we can do something later?
            //but the purpose of what we are building is a form index so we can invalidate an entire form based on failure of children
        }
    }

    function addInvalidatedFieldToFormIndex(node, reason) {
        var parent = getParentForm(node);
        if (parent && parent.length > 0) {
            if (invalidatedFormMembers && invalidatedFormMembers[getUniqueID(parent)]) {
                invalidatedFormMembers[getUniqueID(parent)][getUniqueID(node)] = node;
                if (!invalidReasons[getUniqueID(node)]) {
                    invalidReasons[getUniqueID(node)] = {};
                }
                if (!invalidReasons[getUniqueID(node)][reason]) {
                    invalidReasons[getUniqueID(node)][reason] = reason;
                }
            }
        }
    }

    function removeInvalidatedFieldFromFormIndex(node, reason) {
        var parent = getParentForm(node);
        if (parent && parent.length > 0) {
            if (invalidReasons[getUniqueID(node)] && invalidReasons[getUniqueID(node)][reason]) {
                delete invalidReasons[getUniqueID(node)][reason];
            }
            if (!hasInvalidatedReasons(node) && invalidatedFormMembers && invalidatedFormMembers[getUniqueID(parent)] && invalidatedFormMembers[getUniqueID(parent)][getUniqueID(node)]) {
                delete invalidatedFormMembers[getUniqueID(parent)][getUniqueID(node)];
            }
        }
    }

    function hasInvalidatedReasons(node) {
        if (invalidReasons[getUniqueID(node)]) {
            for (var prop in invalidReasons[getUniqueID(node)]) {
                return true;
            }
        }
        return false;
    }

    function removeInvalidatedReasons(node) {
        if (invalidReasons[getUniqueID(node)]) {
            invalidReasons[getUniqueID(node)] = {};
        }
        removeInvalidatedFieldFromFormIndex(node);
    }

    function getFormByInput(input) {
        var parent = getParentForm(input);
        var arr = [];
        if (parent && parent.length > 0) {
            return parent;
        }
        return parent;
    }

    function getInvalidatedInputsByInput(input, form) {
        var parent;
        if (input) {
            parent = getParentForm(input);
        } else {
            parent = form;
        }
        if (!parent) return;
        //checkPrecondition(parent, "input or form must be defined, nad the parent form must have children");
        var arr = [];
        if (parent && parent.length > 0) {
            if (invalidatedFormMembers && invalidatedFormMembers[getUniqueID(parent)]) {
                for (var prop in invalidatedFormMembers[getUniqueID(parent)]) {
                    arr.push(invalidatedFormMembers[getUniqueID(parent)][prop]);
                }
                return arr;
            }
        }
        return [];
    }

    function getIsFormValidByInput(input) {
        var ii = getInvalidatedInputsByInput(input);
        return ii && ii.length == 0;
    }

    function getParentForm(node) {
        var toReturn = node.closest("form");
        if (toReturn.length == 0) toReturn = null;
        return toReturn;
    }

    function setUniqueID(node) {
        if (node.attr("axa-input-id") == null) {
            node.attr("axa-input-id", getNonceString());
        }
    }

    function getUniqueID(node) {
        try {
            return node.attr("axa-input-id");
        } catch (err) {
            return $(node).attr("axa-input-id");
        }
    }

    function getNonceString() {
        var ran = Math.floor(Math.random() * ((100 - 15) + 1) + 15);
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < ran; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    //revalidation of the form
    function revalidate() {
        invalidReasons = {};
        var hasAtLeastOneInput = false;
        $.each($(":input"), function(index, value) {
            value = $(value);
            if (value.attr("axa-input") != null) {
                var intv = setInterval(function() {
                    evalHandler(null, value, null, null);
                    clearInterval(intv);
                }, 1);
                hasAtLeastOneInput = true;
            }
        });
        //if theres no inputs left in the form then it will show as invalidated unless we specifically tell it its not
        if (!hasAtLeastOneInput) {
            lastWasSuccess = true;
            firstPass = false;
            if ($.fn.inputEval.config.formvalidatedcallback) $.fn.inputEval.config.formvalidatedcallback();
        }

    }


    var revalidationInterval = null;
    //define root of document
    var root = $($(":root")[0]);
    //run a digest function on dom update
    function digest(e) {
        $.each($(":input"), function(index, value) {
            value = $(value);
            if (value.attr("axa-input") != null) {
                if (value.attr("initiallistenersadded") != "true") {
                	var config = getLocalConfig(value);
                    //buildFormIndexes(value);
                    var inputString = "input[name=" + value.attr("name") + "]";
                    var collection = $(inputString);
                    switch (value.attr("type")) {
                        case "input":
                            value.attr("initiallistenersadded", "true");
                            doEventListeners(value, config);
                            break;
                        case "radio":
                            $.each(collection, function(index, item) {
                                if ($(item).attr("initiallistenersadded") != "true") {
                                    $(item).attr("initiallistenersadded", "true");
                                    doEventListeners($(item), config);
                                }
                            });
                            break;
                    }
                    //this is similar to how binding is kicked off manually one time, we want forms validated once at 
                    //the beginning, especially if they are mandatory because this immediately invalidates the form itself
                    evalHandler(null, value, null, config);
                }
            }
        })
        
        //catch any items that have been removed from the dom
        //theyll be readded when they get added to the dom
        for (var key in invalidatedFormMembers) {
            var member = invalidatedFormMembers[key];
            for (var key2 in member) {
                var member2 = member[key2];
                var isVisible = member2.is(":visible");
                var parent = member2[0].parentNode;
                if (!parent || !isVisible) {
                    var toDelete = member[key2]
                    delete member[key2];
                    revalidate();
                }
            }
        }
    }

    function doEventListeners(input, config) {
        if (input.attr("initiallistenersadded") != "true") return;
        input.off(config.triggerevent, evalHandler);
        input.on(config.triggerevent, evalHandler);
    }


    var mostRecentKey;
    $(window).keyup(function(e) {
        mostRecentKey = e.which;
    });

    $(window).keydown(function(e) {
        mostRecentKey = e.which;
        var hasFocus = $("*:focus").is("input");
        var whatHasFocus = $(":focus");
        var focusedIsPartOfInvalidatedForm = !getIsFormValidByInput(whatHasFocus)
        if ($.fn.inputEval.config.preventinvalidatedformsubmissionwithreturnkey == true && e.which == 13 && hasFocus && focusedIsPartOfInvalidatedForm) {
            //event.preventDefault();
            return false;
        }
    });

    //register all of our methods for evaluation - this lets external development also register
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.TO_UPPER_CASE, $.fn.toUpperCase);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.TO_LOWER_CASE, $.fn.toLowerCase);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.MAX_LENGTH, $.fn.maxLength);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.MIN_LENGTH, $.fn.minLength);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.VALIDATE_EMAIL, $.fn.validateEmail);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.MANDATORY, $.fn.mandatory);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.ONLY_NUMBERS, $.fn.onlyNumbers);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.MATCH_FIELD, $.fn.matchField);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.CONDITIONAL_FIELD, $.fn.conditionalField);
    $.fn.registerValidationFunction($.fn.inputEval.INPUT_TYPE_ENUM.CALL_SERVICE, $.fn.callService);

    var to;
  //make sure were not doing multiple revalidations if many items in a form get removed
  //by giving it a small pause we can make sure that if many items are removed well only do
  //one revalidation pass, thus improving performance vastly
    function stagger(e){
    	clearTimeout(to);
    	to = setTimeout(function() {
            digest(e);
            clearTimeout(to);
        }, 1);
    }

    //trigger digest when dom changes
    root.on("DOMSubtreeModified propertychange", stagger);

})(jQuery);