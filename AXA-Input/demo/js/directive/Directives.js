app.directive('collapse', function() {
	return {
		scope : {
			collapse : "="
		},
		link : function(scope, element, attrs) {
			var isFirst = true;
			$(element).css("opacity", 0)
			scope.$watch("collapse", function(val) {
				switch(val){
					case true:
						if(isFirst){$(element).hide(10);$(element).slideUp(100)}else{$(element).slideUp(400)};
						//$(element).slideUp(isFirst ? .1 : 220, isFirst ? function(){$(element).css("opacity", 1)} : null);
						break;
					case false:
						if(isFirst){$(element).show(10);}else{$(element).css("opacity", 1); $(element).slideDown(400)};
						//$(element).slideDown(isFirst ? .1 : 220);
						break;
				}
				isFirst = false;
			})
		}
	};

});


app.directive("zoom", function(){
    return{
        scope:{
            zoom : "=zoom"
        },
        controller:function($scope, $attrs, $element){
            var origSize = $element.css("fontSize")
            var first = true;
            $scope.$watch("zoom", function(oldVal, newVal){
                if(first){
                    first = false;
                    return;
                }
                if(!newVal){
                    $($element).animate({
                        fontSize: "2em",
                        color: "#000000"
                    }, 200 );
                }else{
                    $($element).animate({
                        fontSize: "1em",
                        color: "#686868"
                    }, 200 );
                }
            })
        }
    }
});

app.directive("focus", function(){
    return{
        scope:{
            focus : "=focus"
        },
        controller:function($scope, $attrs, $element){
           $scope.$watch("focus", function(){
        	   if($scope.focus)$($element).focus();
           });
           
        }
    }
});


app.directive("focusfade", function($rootScope){
    return{
        controller:function($scope, $attrs, $element){
           
        	$($element).on("mouseenter", function(){
        		$($element).animate({
        		    opacity: 1
        		  }, 0, function() {
        		    // Animation complete.
        		  });
        	   $rootScope.$broadcast("fadeOut", $element);
           })
           
           $($element).on("mouseleave", function(){
        	   $rootScope.$broadcast("fadeIn", $element);
           })
           
           $rootScope.$on("fadeOut", function(val, val2){
        	   if(val2 == $element)return;
        	   $($element).animate({
       		    opacity: .25
       		  }, 0, function() {
       		    // Animation complete.
       		  });
           })
           
           $rootScope.$on("fadeIn", function(val, val2){
        	   if(val2 == $element)return;
        	   $($element).animate({
       		    opacity: 1
       		  }, 0, function() {
       		    // Animation complete.
       		  });
           })
        }
    }				
});

app.directive("scrolltome", function($interval){
    return{
        scope:{
            scrolltome : "=scrolltome",
            matchvalue : "@matchvalue",
            scrollview : "@scrollview"
        },
        controller:function($scope, $attrs, $element){
           $scope.$watch("scrolltome", function(val){
        	   if(val == $scope.matchvalue){
        		   $interval(function(){
	        		   var sv = $("#"+$scope.scrollview);
	        		   sv.scrollTo($($element), 300);
        		   }, 400, 1);
        	   }
        	   
           });
           
        }
    }
});

app.directive("focusinput", function($interval){
    return{
        scope:{
            input : "=focusinput"
        },
        controller:function($scope, $attrs, $element){
        	$($element).on("mouseup", function(){
        		var input = $($scope.input);
        		if(input){
    				input.focus();
        		}
        	})
        }
    }
});


        