
var app = angular.module('axaInputExample', ["ngMaterial", "ngSanitize", 'ngMdIcons', "angular-clipboard"]);
    

app.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('blue')
        .accentPalette('blue').
        warnPalette('red').
        backgroundPalette('grey');

    $mdThemingProvider.theme('dark', 'default')
        .primaryPalette('red')
        .accentPalette('blue')
        . warnPalette('blue').
        backgroundPalette('blue');

});