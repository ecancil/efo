QUnit.test( "Test Capitalization", function( assert ) {
    var done = assert.async();
	$.fn.inputEval.config.passcallback = function (input, oldVal, newVal){
		assert.ok($("#theInput").val() == "CAPS");
		$.fn.inputEval.config.passcallback = null;
		done();
	}
    var root = $($(":root")[0]);
	root.append("<input id='theInput' type='input' axa-input='touppercase' autoMutateValue='true'></input>");
	$("#theInput").changeVal("caps");
});

QUnit.test( "Test Lower Case", function( assert ) {
    var done = assert.async();
	$.fn.inputEval.config.passcallback = function (input, oldVal, newVal){
		assert.ok($("#theInput2").val() == "caps");
		$.fn.inputEval.config.passcallback = null;
		done();
	}
    var root = $($(":root")[0]);
	root.append("<input id='theInput2' type='input' axa-input='tolowercase' autoMutateValue='true'></input>");
	$("#theInput2").changeVal("CAPS");
});

QUnit.test( "Test Lower Case and length", function( assert ) {
    var done = assert.async();
    var counter = 0;
	$.fn.inputEval.config.passcallback = function (input, oldVal, newVal){
		//were applying two things so it will make two sweeps
		if(counter == 0){
			assert.ok($("#theInput3").val() == "caps");
			counter ++;
		}else if(counter == 1){
			assert.ok($("#theInput3").val() == "cap");
			$.fn.inputEval.config.passcallback = null;
			done();
		}
	}
    var root = $($(":root")[0]);
	root.append("<input id='theInput3' type='input' axa-input='tolowercase limitcharacterlength' autoMutateValue='true' maxcharacterlength='3'></input>");
	$("#theInput3").changeVal("CAPS");
});

QUnit.test( "Test Capitalization javascript", function( assert ) {
    var done = assert.async();
	$.fn.inputEval.config.passcallback = function (input, oldVal, newVal){
		assert.ok($("#theInput4").val() == "CAPS");
		$.fn.inputEval.config.passcallback = null;
		done();
	}
    var root = $($(":root")[0]);
	root.append("<input id='theInput4' type='input'></input>");
	$.fn.toUpperCase($("#theInput4"));
	$("#theInput4").changeVal("caps");
});




//helpers
//lets trigger a chance even tho javascript is changing it
$.fn.changeVal = function(v){
	return $(this).val(v).trigger("change");
}















